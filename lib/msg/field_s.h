/*
 * Copyright © 2018 - Vitaliy Perevertun
 *
 * This file is part of journal
 *
 * This file is licensed under the MIT license.
 * See the file LICENSE.
 */

#ifndef _JOURNAL_MSG_FIELD_H_
#define _JOURNAL_MSG_FIELD_H_

#include <stdlib.h>
#include <stdint.h>


struct field
{
	uint64_t	hash;

	size_t		len;
	uint8_t		value;
};

#endif	/* _JOURNAL_MSG_FIELD_H_ */
